﻿namespace OtisClientes.Utility
{
    public static class SweetAlert2
    {
        #region Alerta
        /// <summary>
        /// Lista de tipo de alertas validas de Sweet Alert v2
        /// </summary>
        private enum TipoAlerta
        {
            success,
            error,
            warning,
            info,
            question
        }
        /// <summary>
        /// Alerta con la version normal.
        /// </summary>
        /// <param name="titulo">Titulo de la alerta</param>
        /// <param name="msg">Mensaje de la alerta</param>
        /// <param name="alerta">Tipo de alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        private static string Swal(string titulo, string msg, TipoAlerta alerta)
        {
            return $"Swal.fire({{title: '{titulo}', text: '{msg}', icon: '{alerta}' }});";
        }
        /// <summary>
        /// Alerta con la version extendida de tipo toast.
        /// </summary>
        /// <param name="titulo">Titulo de la alerta</param>
        /// <param name="alerta">Tipo de alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        private static string SwalMixin(string titulo, TipoAlerta alerta)
        {
            return $"Swal.mixin({{ toast: true, position: 'top-end', showConfirmButton: false, timer: 2000, timerProgressBar: true }}).fire({{icon: '{alerta}', title: '{titulo}' }});";
        }
        #endregion
        #region Success
        /// <summary>
        /// Alerta de exito por defecto.
        /// </summary>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string Success()
        {
            return Swal("Guardado", "Los datos fueron guardados con éxito.", TipoAlerta.success);
        }
        /// <summary>
        /// Alerta de exito con parametros.
        /// </summary>
        /// <param name="titulo">Titulo de la alerta</param>
        /// /// <param name="msg">Mensaje de la alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string Success(string titulo, string msg)
        {
            return Swal(titulo, msg, TipoAlerta.success);
        }
        /// <summary>
        /// Alerta de exito de tipo toast
        /// </summary>
        /// <param name="msg">Mensaje de la alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string SuccessMixin(string msg)
        {
            return SwalMixin(msg, TipoAlerta.success);
        }
        #endregion
        #region Error
        /// <summary>
        /// Alerta de error por defecto.
        /// </summary>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string Error()
        {
            return Swal("Error", "Ha ocurrido un error.", TipoAlerta.error);
        }
        /// <summary>
        /// Alerta de error con parametros.
        /// </summary>
        /// <param name="titulo">Titulo de la alerta</param>
        /// <param name="msg">Mensaje de la alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string Error(string titulo, string msg)
        {
            return Swal(titulo, msg, TipoAlerta.error);
        }
        /// <summary>
        /// Alerta de error de tipo toast
        /// </summary>
        /// <param name="msg">Mensaje de la alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string ErrorMixin(string msg)
        {
            return SwalMixin(msg, TipoAlerta.error);
        }
        #endregion
        #region Warning
        /// <summary>
        /// Alerta de peligro por defecto.
        /// </summary>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string Warning()
        {
            return Swal("Advertencia", "Ha ocurrido un error.", TipoAlerta.warning);
        }
        /// <summary>
        /// Alerta de peligro con parametros.
        /// </summary>
        /// <param name="titulo">Titulo de la alerta</param>
        /// <param name="msg">Mensaje de la alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string Warning(string titulo, string msg)
        {
            return Swal(titulo, msg, TipoAlerta.warning);
        }
        /// <summary>
        /// Alerta de peligro de tipo toast.
        /// </summary>
        /// <param name="msg">Mensaje de la alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string WarningMixin(string msg)
        {
            return SwalMixin(msg, TipoAlerta.warning);
        }
        #endregion
        #region Info
        /// <summary>
        /// Alerta de informacion con un parametro.
        /// </summary>
        /// <param name="msg">Mensaje de la alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string Info(string msg)
        {
            return Swal("Informacion", msg, TipoAlerta.info);
        }
        /// <summary>
        /// Alerta de informacion con dos parametros.
        /// </summary>
        /// <param name="titulo">Titulo de la alerta</param>
        /// <param name="msg">Mensaje de la alerta</param>
        /// <returns>Codigo JavaScript de la alerta de tipo string</returns>
        public static string Info(string titulo, string msg)
        {
            return Swal(titulo, msg, TipoAlerta.info);
        }
        /// <summary>
        /// Alerta de informacion de tipo toast
        /// </summary>
        /// <param name="msg">Mensaje de la alerta</param>
        /// <returns></returns>
        public static string InfoMixin(string msg)
        {
            return SwalMixin(msg, TipoAlerta.info);
        }
        #endregion
        #region Question
        /// <summary>
        /// Alerta de Pregunta (Customizacion pendiente)
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string Question(string msg)
        {
            return Swal("Pregunta", msg, TipoAlerta.question);
        }
        #endregion
    }
}
