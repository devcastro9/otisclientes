﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [Table("TipoPersona")]
    public partial class TipoPersona
    {
        public TipoPersona()
        {
            Clientes = new HashSet<Cliente>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTipoPersona { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string PersonaTipo { get; set; } = null!;
        [StringLength(200)]
        [Unicode(false)]
        public string DescripcionCorta { get; set; } = null!;
        [StringLength(450)]
        [Unicode(false)]
        public string Definicion { get; set; } = null!;

        [InverseProperty("IdTipoPersonaNavigation")]
        public virtual ICollection<Cliente> Clientes { get; set; }
    }
}
