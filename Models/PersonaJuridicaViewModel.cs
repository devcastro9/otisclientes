﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [NotMapped]
    public class PersonaJuridicaViewModel
    {
        //[Key]
        public int IdPersona { get; set; }
        [Display(Name = "Razon Social")]
        [StringLength(200)]
        public string RazonSocial { get; set; } = null!;
        [Display(Name = "Documento de Identidad")]
        public int IdDocumentoIdentidad { get; set; }
        [Display(Name = "NIT")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "El valor no es valido")]
        [StringLength(15)]
        public string NumeroDocumento { get; set; } = null!;
        [Display(Name = "Celular")]
        [Range(1, 2147483645, ErrorMessage = "Valor no admitido")]
        public int Celular { get; set; }
        [Display(Name = "Correo electronico")]
        [StringLength(200)]
        [EmailAddress]
        public string CorreoElectronico { get; set; } = null!;
    }
}
