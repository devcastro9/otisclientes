﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [Table("Edificio")]
    public partial class Edificio
    {
        public Edificio()
        {
            Clientes = new HashSet<Cliente>();
        }

        [Key]
        [StringLength(21)]
        [Unicode(false)]
        public string IdCodigoEdificio { get; set; } = null!;
        [StringLength(21)]
        [Unicode(false)]
        public string CodigoCorto { get; set; } = null!;
        [StringLength(100)]
        [Unicode(false)]
        public string Descripcion { get; set; } = null!;
        public int IdDepartamento { get; set; }
        public int IdEstadoRegistro { get; set; }

        [InverseProperty("IdCodigoEdificioNavigation")]
        public virtual ICollection<Cliente> Clientes { get; set; }
    }
}
