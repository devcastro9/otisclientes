﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [Table("Comportamiento")]
    public partial class Comportamiento
    {
        public Comportamiento()
        {
            Clientes = new HashSet<Cliente>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdComportamiento { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string Descripcion { get; set; } = null!;

        [InverseProperty("IdComportamientoNavigation")]
        public virtual ICollection<Cliente> Clientes { get; set; }
    }
}
