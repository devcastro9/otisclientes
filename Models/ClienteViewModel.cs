﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [NotMapped]
    public class ClienteViewModel
    {
        public PersonaNaturalViewModel? PNatural { get; set; }
        public PersonaJuridicaViewModel? PJuridica { get; set; }
    }
}
