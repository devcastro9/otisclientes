﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [NotMapped]
    public class InputViewModel
    {
        [Display(Name = "Código de Cliente")]
        [Required(ErrorMessage = "El código de cliente es incorrecto.")]
        [Comment("UserName de la tabla AspNetUsers")]
        public string UserName { get; set; } = null!;

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "La contraseña es inválida.")]
        [DataType(DataType.Password)]
        [Comment("PasswordHash de la tabla AspNetUsers")]
        public string Password { get; set; } = null!;
    }
}
