﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [Table("Estado")]
    public partial class Estado
    {
        public Estado()
        {
            Clientes = new HashSet<Cliente>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdEstadoRegistro { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? Descripcion { get; set; }

        [InverseProperty("IdEstadoRegistroNavigation")]
        public virtual ICollection<Cliente> Clientes { get; set; }
    }
}
