﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [Table("DocumentoIdentidad")]
    public partial class DocumentoIdentidad
    {
        public DocumentoIdentidad()
        {
            Clientes = new HashSet<Cliente>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdDocumentoIdentidad { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string Descripcion { get; set; } = null!;
        public int Filtro { get; set; }

        [InverseProperty("IdDocumentoIdentidadNavigation")]
        public virtual ICollection<Cliente> Clientes { get; set; }
    }
}
