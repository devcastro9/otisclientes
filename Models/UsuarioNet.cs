﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace OtisClientes.Models
{
    public class UsuarioNet : IdentityUser
    {
        [StringLength(21)]
        [Unicode(false)]
        public string CodigoEdificio { get; set; } = null!;

        public int IdDepartamento { get; set; }
    }
}
