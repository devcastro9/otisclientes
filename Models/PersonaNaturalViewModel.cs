﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [NotMapped]
    public class PersonaNaturalViewModel
    {
        //[Key]
        public int IdPersona { get; set; }
        [Display(Name = "Apellido Paterno")]
        [StringLength(100)]
        public string PrimerApellido { get; set; } = null!;
        [Display(Name = "Apellido Materno")]
        [StringLength(100)]
        public string SegundoApellido { get; set; } = null!;
        [Display(Name = "Nombres")]
        [StringLength(100)]
        public string Nombres { get; set; } = null!;
        [Display(Name = "Tipo de Documento de Identidad")]
        public int IdDocumentoIdentidad { get; set; }
        [Display(Name = "Numero del Documento de Identidad")]
        [RegularExpression(@"^[0-9A-Z]+$", ErrorMessage = "El valor no es valido")]
        [StringLength(15)]
        public string NumeroDocumento { get; set; } = null!;
        [Display(Name = "Celular")]
        [Range(1, 2147483645, ErrorMessage = "Valor no admitido")]
        public int Celular { get; set; }
        [Display(Name = "Correo electronico")]
        [EmailAddress]
        public string CorreoElectronico { get; set; } = null!;
    }
}
