﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtisClientes.Models
{
    [Table("Cliente")]
    public partial class Cliente
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCliente { get; set; }
        [Display(Name = "Codigo cliente")]
        [StringLength(21)]
        [Unicode(false)]
        public string IdCodigoEdificio { get; set; } = null!;
        [Display(Name = "Tipo de persona")]
        public int IdTipoPersona { get; set; }
        [Display(Name = "Apellido Paterno")]
        [StringLength(100)]
        [Unicode(false)]
        public string? PrimerApellido { get; set; }
        [Display(Name = "Apellido Materno")]
        [StringLength(100)]
        [Unicode(false)]
        public string? SegundoApellido { get; set; }
        [Display(Name = "Nombre(s)")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Nombres { get; set; }
        [Display(Name = "Razon Social")]
        [StringLength(200)]
        [Unicode(false)]
        public string? RazonSocial { get; set; }
        [Display(Name = "Tipo documento identidad")]
        public int IdDocumentoIdentidad { get; set; }
        [Display(Name = "Numero de Documento")]
        [StringLength(15)]
        [Unicode(false)]
        public string NumeroDocumento { get; set; } = null!;
        public int Celular { get; set; }
        [Display(Name = "Correo electronico")]
        [StringLength(200)]
        [Unicode(false)]
        public string CorreoElectronico { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime FechaRegistro { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaModificacion { get; set; }
        public int IdEstadoRegistro { get; set; }
        public int IdComportamiento { get; set; }

        [ForeignKey("IdCodigoEdificio")]
        [InverseProperty("Clientes")]
        public virtual Edificio IdCodigoEdificioNavigation { get; set; } = null!;
        [ForeignKey("IdComportamiento")]
        [InverseProperty("Clientes")]
        public virtual Comportamiento IdComportamientoNavigation { get; set; } = null!;
        [ForeignKey("IdDocumentoIdentidad")]
        [InverseProperty("Clientes")]
        public virtual DocumentoIdentidad? IdDocumentoIdentidadNavigation { get; set; }
        [ForeignKey("IdEstadoRegistro")]
        [InverseProperty("Clientes")]
        public virtual Estado IdEstadoRegistroNavigation { get; set; } = null!;
        [ForeignKey("IdTipoPersona")]
        [InverseProperty("Clientes")]
        public virtual TipoPersona? IdTipoPersonaNavigation { get; set; }
    }
}
