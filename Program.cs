using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OtisClientes.Data;
using OtisClientes.Models;
using OtisClientes.Services;
using System;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Contexto de la base de datos y cadena de conexion
builder.Services.AddDbContext<OTISClientesContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("ServerProduction")));
// Seguridad
builder.Services.AddIdentity<UsuarioNet, IdentityRole>(options =>
{
    // Parametros de password (dureza del password)
    options.Password.RequiredLength = 0;
    options.Password.RequireUppercase = false;
    options.Password.RequireLowercase = false;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequiredUniqueChars = 0;
    options.Password.RequireDigit = false;
}).AddEntityFrameworkStores<OTISClientesContext>();

// Add services to the container.
builder.Services.AddControllersWithViews();
// Http Context (Info)
builder.Services.AddHttpContextAccessor();
// Dependency Inyection Service: UserInfo (De tipo trasient)
builder.Services.AddTransient<IUserInfo, UserInfo>();
// Cookies
builder.Services.ConfigureApplicationCookie(options =>
{
    // Cookie settings
    options.Cookie.Name = "34c3jj973hjkop2fc937";
    options.Cookie.HttpOnly = true;
    options.ExpireTimeSpan = TimeSpan.FromSeconds(300);
    // Opciones de Login
    options.LoginPath = "/Auth";
    options.AccessDeniedPath = "/Auth";
    options.SlidingExpiration = true;
});
//builder.Services.AddAntiforgery(options =>
//{
//    options.Cookie.Name = "X-CSRF-TOKEN";
//    options.Cookie.Expiration = TimeSpan.FromSeconds(3);
//});
// Construccion con las opciones cargadas
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
// Autenticacion (Seguridad)
app.UseAuthentication();
// Autorizacion (Seguridad)
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
