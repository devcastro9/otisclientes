﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OtisClientes.Models;

namespace OtisClientes.Data
{
    public partial class OTISClientesContext : IdentityDbContext<UsuarioNet>
    {
        public OTISClientesContext(DbContextOptions<OTISClientesContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cliente> Clientes { get; set; } = null!;
        public virtual DbSet<Comportamiento> Comportamientos { get; set; } = null!;
        public virtual DbSet<DocumentoIdentidad> DocumentoIdentidades { get; set; } = null!;
        public virtual DbSet<Edificio> Edificios { get; set; } = null!;
        public virtual DbSet<Estado> Estados { get; set; } = null!;
        public virtual DbSet<TipoPersona> TipoPersonas { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Al sobreescribir el metodo de OnModelCreating de IdentityDbContext
            // para agregar la construccion de los modelos
            // se anulan las creaciones de los modelos de Asp Net Core Identity
            base.OnModelCreating(modelBuilder);
            // Construccion de modelos
            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.Property(e => e.Celular).HasDefaultValueSql("((0))");

                entity.Property(e => e.CorreoElectronico).HasDefaultValueSql("('')");

                entity.Property(e => e.FechaModificacion).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaRegistro).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdCodigoEdificio).HasDefaultValueSql("('-1')");

                entity.Property(e => e.IdComportamiento).HasDefaultValueSql("((3))");

                entity.Property(e => e.IdDocumentoIdentidad).HasDefaultValueSql("((0))");

                entity.Property(e => e.IdEstadoRegistro).HasDefaultValueSql("((3))");

                entity.Property(e => e.IdTipoPersona).HasDefaultValueSql("((0))");

                entity.Property(e => e.Nombres).HasDefaultValueSql("('')");

                entity.Property(e => e.NumeroDocumento).HasDefaultValueSql("('')");

                entity.Property(e => e.PrimerApellido).HasDefaultValueSql("('')");

                entity.Property(e => e.RazonSocial).HasDefaultValueSql("('')");

                entity.Property(e => e.SegundoApellido).HasDefaultValueSql("('')");

                entity.HasOne(d => d.IdCodigoEdificioNavigation)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.IdCodigoEdificio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cliente_Edificio");

                entity.HasOne(d => d.IdComportamientoNavigation)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.IdComportamiento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cliente_Comportamiento");

                entity.HasOne(d => d.IdDocumentoIdentidadNavigation)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.IdDocumentoIdentidad)
                    .HasConstraintName("FK_Cliente_DocumentoIdentidad");

                entity.HasOne(d => d.IdEstadoRegistroNavigation)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.IdEstadoRegistro)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cliente_Estado");

                entity.HasOne(d => d.IdTipoPersonaNavigation)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.IdTipoPersona)
                    .HasConstraintName("FK_Cliente_TipoPersona");
            });

            modelBuilder.Entity<Comportamiento>(entity =>
            {
                entity.Property(e => e.Descripcion).HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<DocumentoIdentidad>(entity =>
            {
                entity.Property(e => e.Descripcion).HasDefaultValueSql("('')");
                entity.Property(e => e.Filtro).HasDefaultValueSql("((0))");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
