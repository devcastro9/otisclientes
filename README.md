# Aplicacion Web Clientes OTIS Bolivia
Aplicacion web para clientes.
## Comandos para docker
Construir:
```
sudo docker build -t aspnet-image -f Dockerfile .
```
Correr contenedor:
```
sudo docker run -d -p 5000:80 -p 5001:443 --name otisapp aspnet-image
```

Crear contenedor:
```
sudo docker create --name clientes-cgi aspnet-image
```
Borrar cosas innecesarias:
```
sudo docker system prune
```
Borrar cosas innecesarias (purgado):
```
sudo docker system prune -a
```