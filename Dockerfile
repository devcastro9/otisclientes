FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["OtisClientes.csproj", "."]
RUN dotnet restore "./OtisClientes.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "OtisClientes.csproj" -c Release -o /app/build --nologo

FROM build AS publish
RUN dotnet publish "OtisClientes.csproj" -c Release -o /app/publish --nologo

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "OtisClientes.dll"]