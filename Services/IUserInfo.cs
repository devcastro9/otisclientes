﻿using System.Threading.Tasks;

namespace OtisClientes.Services
{
    public interface IUserInfo
    {
        public Task<string> GetEdificio();
        public bool InRole(string rolename);
    }
}
