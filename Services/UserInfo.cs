﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using OtisClientes.Data;
using System.Linq;
using System.Threading.Tasks;

namespace OtisClientes.Services
{
    public class UserInfo : IUserInfo
    {
        private readonly OTISClientesContext _context;
        private readonly IHttpContextAccessor _http;
        public UserInfo(OTISClientesContext context, IHttpContextAccessor http)
        {
            _context = context;
            _http = http;
        }
        public async Task<string> GetEdificio()
        {
            if (_http.HttpContext == null || _http.HttpContext.User.Identity == null)
            {
                return "-1";
            }
            string username = _http.HttpContext.User.Identity.Name ?? "";
            if (username.Length > 1)
            {
                string? result = await (from e in _context.Edificios where e.CodigoCorto == username select e.IdCodigoEdificio).FirstOrDefaultAsync();
                return result ?? "-1";
            }
            return "-1";
        }
        public bool InRole(string rolename)
        {
            return _http.HttpContext != null && _http.HttpContext.User.IsInRole(rolename);
        }
    }
}
