﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OtisClientes.Data;
using OtisClientes.Models;
using System.Linq;
using System.Threading.Tasks;

namespace OtisClientes.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class AdministrationController : Controller
    {
        private readonly UserManager<UsuarioNet> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly OTISClientesContext _context;
        private readonly ILogger<AuthController> _logger;
        public AdministrationController(UserManager<UsuarioNet> userManager, RoleManager<IdentityRole> roleManager, OTISClientesContext context, ILogger<AuthController> logger)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _logger = logger;
        }
        // Usuarios
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _userManager.Users.ToListAsync());
        }
        // Creacion de usuarios
        [HttpGet]
        public IActionResult CreateUser()
        {
            return View();
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateUser([Bind("UserName,IdDepartamento")] UsuarioNet usuario)
        {
            //usuario.Email = "admin@gmail.com";
            usuario.CodigoEdificio = "-1";
            string password = "User123*";
            IdentityResult result = await _userManager.CreateAsync(usuario, password);
            if (result.Succeeded)
            {
                //_ = await _userManager.AddToRoleAsync(usuario, "Administrador");
                //_logger.LogInformation($"Usuario {usuario.UserName} con rol");
                return RedirectToAction(nameof(Index));
            }
            else
            {
                //_logger.LogInformation($"Usuario {usuario.UserName} no pudo ser creado");
                return View(usuario);
            }
        }
        // Roles
        [HttpGet]
        public async Task<IActionResult> Roles()
        {
            _logger.LogInformation("Ver roles");
            return View(await _roleManager.Roles.OrderBy(x => x.NormalizedName).ToListAsync());
        }
        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateRole([Bind("Name")] IdentityRole rol)
        {
            IdentityResult result = await _roleManager.CreateAsync(rol);
            if (result.Succeeded)
            {
                //_logger.LogInformation($"Rol: {rol} creado");
                return RedirectToAction(nameof(Roles));
            }
            else
            {
                //_logger.LogWarning("Error al crear rol");
                return View(rol);
            }
        }
        //Cargar clientes en masa
        //[HttpPost]
        // public async Task<IActionResult> CargarClientes()
        // {
        //     //var result = await _userManager.CreateAsync(new UsuarioNet { Email = "visitante@gmail.com", UserName = "54321", CodigoEdificio = "-1", IdDepartamento = 5 });
        //     //if (result.Succeeded)
        //     //{
        //     //    var visitante = await _userManager.FindByEmailAsync("visitante@gmail.com");
        //     //    await _userManager.AddToRoleAsync(visitante, "Cliente");
        //     //}
        //     //var result2 = await _userManager.CreateAsync(new UsuarioNet { Email= "admin@gmail.com", UserName = "admin", CodigoEdificio = "-1", IdDepartamento = 5 });
        //     //if (result.Succeeded)
        //     //{
        //     //    var admin = await _userManager.FindByEmailAsync("admin@gmail.com");
        //     //    await _userManager.AddToRoleAsync(admin, "Administrador");
        //     //}

        //     // Caso duplicado Codigo Corto de Edificio: 10997
        //     // Crear Rol Administrador
        //     //await _roleManager.CreateAsync(new IdentityRole("Administrador"));
        //     // Crear Rol Cliente
        //     //await _roleManager.CreateAsync(new IdentityRole("Cliente"));
        //     // Cargar Edificios a crear usuarios
        //     //List<Edificio> edificios = await _context.Edificios.Where(m => m.IdCodigoEdificio == "20105-10997").AsNoTracking().ToListAsync();
        //     //foreach (var item in edificios)
        //     //{
        //     //    UsuarioNet usuario = new() { UserName = item.CodigoCorto, CodigoEdificio = item.IdCodigoEdificio, IdDepartamento = item.IdDepartamento };
        //     //    IdentityResult result = await _userManager.CreateAsync(usuario);
        //     //    if (result.Succeeded)
        //     //    {
        //     //        IdentityResult roladded = await _userManager.AddToRoleAsync(usuario, "Cliente");
        //     //        if (roladded.Succeeded)
        //     //        {
        //     //            _logger.LogInformation($"Usuario {usuario.UserName} con rol");
        //     //        }
        //     //    }
        //     //}
        //     return Ok();
        // }
        //[HttpPost]
        //public async Task<IActionResult> CambiarPassword()
        //{
        //    try
        //    {
        //        IList<UsuarioNet>? usuariosClientes = await _userManager.GetUsersInRoleAsync("Administrador");
        //        foreach (UsuarioNet? item in usuariosClientes)
        //        {
        //            IdentityResult result = await _userManager.AddPasswordAsync(item, item.UserName);
        //            //IdentityResult result = await _userManager.ChangePasswordAsync(item, "", item.UserName);
        //            if (!result.Succeeded)
        //            {
        //                _logger.LogWarning(message: $"Ocurrio un error {item.UserName}");
        //            }
        //        }
        //    }
        //    catch (System.Exception)
        //    {
        //        throw;
        //    }
        //    return Ok();
        //}
    }
}
