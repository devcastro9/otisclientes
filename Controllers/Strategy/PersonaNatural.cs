﻿using OtisClientes.Data;
using OtisClientes.Models;
using OtisClientes.Services;
using System;
using System.Threading.Tasks;

namespace OtisClientes.Controllers.Strategy
{
    public class PersonaNatural : IPersonaStrategy
    {
        public async Task AddAsync(ClienteViewModel clienteVM, OTISClientesContext context, IUserInfo userinfo)
        {
            Cliente clienteNatural = new()
            {
                IdCodigoEdificio = await userinfo.GetEdificio(),
                IdTipoPersona = 1,
                PrimerApellido = clienteVM.PNatural!.PrimerApellido,
                SegundoApellido = clienteVM.PNatural.SegundoApellido,
                Nombres = clienteVM.PNatural.Nombres,
                IdDocumentoIdentidad = clienteVM.PNatural.IdDocumentoIdentidad,
                NumeroDocumento = clienteVM.PNatural.NumeroDocumento,
                Celular = clienteVM.PNatural.Celular,
                CorreoElectronico = clienteVM.PNatural.CorreoElectronico,
                FechaRegistro = DateTime.Now,
                FechaModificacion = DateTime.Now,
                IdEstadoRegistro = userinfo.InRole("Cliente") ? 1 : 2,
                IdComportamiento = 1
            };
            context.Add(clienteNatural);
            await context.SaveChangesAsync();
        }
        public async Task EditAsync(ClienteViewModel clienteVM, OTISClientesContext context, IUserInfo userinfo)
        {
            Cliente? result = await context.Clientes.FindAsync(clienteVM.PNatural!.IdPersona);
            if (result != null)
            {
                result.IdTipoPersona = 1;
                result.PrimerApellido = clienteVM.PNatural.PrimerApellido;
                result.SegundoApellido = clienteVM.PNatural.SegundoApellido;
                result.Nombres = clienteVM.PNatural.Nombres;
                result.RazonSocial = "";
                result.IdDocumentoIdentidad = clienteVM.PNatural.IdDocumentoIdentidad;
                result.NumeroDocumento = clienteVM.PNatural.NumeroDocumento;
                result.CorreoElectronico = clienteVM.PNatural.CorreoElectronico;
                result.Celular = clienteVM.PNatural.Celular;
                result.FechaModificacion = DateTime.Now;
                context.Update(result);
            }
            await context.SaveChangesAsync();
        }
    }
}
