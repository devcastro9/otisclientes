﻿using OtisClientes.Data;
using OtisClientes.Models;
using OtisClientes.Services;
using System;
using System.Threading.Tasks;

namespace OtisClientes.Controllers.Strategy
{
    public class PersonaJuridica : IPersonaStrategy
    {
        public async Task AddAsync(ClienteViewModel clienteVM, OTISClientesContext context, IUserInfo userinfo)
        {
            Cliente clienteJuridico = new()
            {
                IdCodigoEdificio = await userinfo.GetEdificio(),
                IdTipoPersona = 2,
                RazonSocial = clienteVM.PJuridica!.RazonSocial,
                IdDocumentoIdentidad = clienteVM.PJuridica.IdDocumentoIdentidad,
                NumeroDocumento = clienteVM.PJuridica.NumeroDocumento,
                Celular = clienteVM.PJuridica.Celular,
                CorreoElectronico = clienteVM.PJuridica.CorreoElectronico,
                FechaRegistro = DateTime.Now,
                FechaModificacion = DateTime.Now,
                IdEstadoRegistro = userinfo.InRole("Cliente") ? 1 : 2,
                IdComportamiento = 1
            };
            context.Add(clienteJuridico);
            await context.SaveChangesAsync();
        }
        public async Task EditAsync(ClienteViewModel clienteVM, OTISClientesContext context, IUserInfo userinfo)
        {
            Cliente? result = await context.Clientes.FindAsync(clienteVM.PJuridica!.IdPersona);
            if (result != null)
            {
                result.IdTipoPersona = 2;
                result.PrimerApellido = "";
                result.SegundoApellido = "";
                result.Nombres = "";
                result.RazonSocial = clienteVM.PJuridica.RazonSocial;
                result.IdDocumentoIdentidad = clienteVM.PJuridica.IdDocumentoIdentidad;
                result.NumeroDocumento = clienteVM.PJuridica.NumeroDocumento;
                result.CorreoElectronico = clienteVM.PJuridica.CorreoElectronico;
                result.Celular = clienteVM.PJuridica.Celular;
                result.FechaModificacion = DateTime.Now;
                context.Update(result);
            }
            await context.SaveChangesAsync();
        }
    }
}
