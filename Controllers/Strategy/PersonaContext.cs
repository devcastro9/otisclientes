﻿using OtisClientes.Data;
using OtisClientes.Models;
using OtisClientes.Services;
using System.Threading.Tasks;

namespace OtisClientes.Controllers.Strategy
{
    public class PersonaContext
    {
        private readonly IPersonaStrategy _strategy;
        public PersonaContext(IPersonaStrategy strategy)
        {
            _strategy = strategy;
        }
        public async Task Add(ClienteViewModel clienteVM, OTISClientesContext context, IUserInfo userinfo)
        {
            await _strategy.AddAsync(clienteVM, context, userinfo);
        }

        public async Task Edit(ClienteViewModel clienteVM, OTISClientesContext context, IUserInfo userinfo)
        {
            await _strategy.EditAsync(clienteVM, context, userinfo);
        }
    }
}
