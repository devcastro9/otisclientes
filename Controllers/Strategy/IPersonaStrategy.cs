﻿using OtisClientes.Data;
using OtisClientes.Models;
using OtisClientes.Services;
using System.Threading.Tasks;

namespace OtisClientes.Controllers.Strategy
{
    public interface IPersonaStrategy
    {
        public Task AddAsync(ClienteViewModel clienteVM, OTISClientesContext context, IUserInfo userinfo);
        public Task EditAsync(ClienteViewModel clienteVM, OTISClientesContext context, IUserInfo userinfo);
    }
}
