﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OtisClientes.Models;
using OtisClientes.Utility;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace OtisClientes.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private readonly SignInManager<UsuarioNet> _signInManager;
        private readonly UserManager<UsuarioNet> _userManager;
        private readonly ILogger<AuthController> _logger;
        public AuthController(SignInManager<UsuarioNet> signInManager, UserManager<UsuarioNet> userManager, ILogger<AuthController> logger)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
        }
        // Login
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Index([Bind("UserName,Password")] InputViewModel input)
        {
            if (ModelState.IsValid)
            {
                SignInResult result = await _signInManager.PasswordSignInAsync(input.UserName, input.Password, false, false);
                if (result.Succeeded)
                {
                    TempData["AlertMsg"] = SweetAlert2.SuccessMixin("Autenticacion correcta");
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Autenticación fallida, intente otra vez.");
                    _logger.LogWarning("Autenticacion fallida");
                    return View(input);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Ocurrió un error en la autenticación.");
                _logger.LogWarning("Modelo no valido");
                return View(input);
            }
        }
        // Logout
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> LogoutSession()
        {
            await _signInManager.SignOutAsync();
            TempData["AlertMsg"] = SweetAlert2.InfoMixin("La sesion ha finalizado.");
            return LocalRedirect(Url.Content("~/"));
        }
    }
}
