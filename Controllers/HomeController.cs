﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OtisClientes.Controllers.Strategy;
using OtisClientes.Data;
using OtisClientes.Models;
using OtisClientes.Services;
using OtisClientes.Utility;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace OtisClientes.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly OTISClientesContext _context;
        private readonly IUserInfo _userInfo;
        private readonly ILogger<HomeController> _logger;

        public HomeController(OTISClientesContext context, IUserInfo userInfo, ILogger<HomeController> logger)
        {
            _context = context;
            _userInfo = userInfo;
            _logger = logger;
        }
        // Datos de Clientes
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            _logger.LogInformation("Ejecutado Home/Index...");
            IQueryable<Cliente> result = _context.Clientes;
            if (HttpContext.User.IsInRole("Cliente"))
            {
                string IdEdificio = await _userInfo.GetEdificio();
                result = result.Where(m => m.IdCodigoEdificio == IdEdificio && m.IdEstadoRegistro < 5);
            }
            else
            {
                result = result.Where(m => m.IdEstadoRegistro < 5);
            }
            return View(await result.AsNoTracking().ToListAsync());
        }
        // Detalles
        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            _logger.LogInformation("Ejecutado Home/Details");
            if (id == null || _context.Clientes == null)
            {
                return NotFound();
            }
            Cliente? cliente = await _context.Clientes.Include(x => x.IdTipoPersonaNavigation).AsNoTracking().FirstOrDefaultAsync(m => m.IdCliente == id);
            return cliente == null ? NotFound() : View(cliente);
        }
        // CREAR
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            _logger.LogInformation("Ejecutado Home/Create");
            await GetDataAsync("Create");
            return View();
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClienteViewModel persona)
        {
            if (!ModelState.IsValid)
            {
                await GetDataAsync("Create");
                TempData["AlertMsg"] = SweetAlert2.Error();
                return View(nameof(Create), persona);
            }
            else
            {
                var strategy = persona.PJuridica == null ? new PersonaContext(new PersonaNatural()) : new PersonaContext(new PersonaJuridica());
                await strategy.Add(persona, _context, _userInfo);
                TempData["AlertMsg"] = SweetAlert2.Success();
                return RedirectToAction(nameof(Index));
            }
        }
        // EDITAR
        // GET: Clientes/Edit/1
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Clientes == null)
            {
                return NotFound();
            }
            Cliente? cliente = await _context.Clientes.FindAsync(id);
            if (cliente == null)
            {
                return NotFound();
            }
            if (cliente.IdTipoPersona == 1)
            {
                PersonaNaturalViewModel natural = new()
                {
                    IdPersona = cliente.IdCliente,
                    PrimerApellido = cliente.PrimerApellido ?? "",
                    SegundoApellido = cliente.SegundoApellido ?? "",
                    Nombres = cliente.Nombres ?? "",
                    IdDocumentoIdentidad = cliente.IdDocumentoIdentidad,
                    NumeroDocumento = cliente.NumeroDocumento,
                    CorreoElectronico = cliente.CorreoElectronico,
                    Celular = cliente.Celular
                };
                await GetDataAsync("Edit", id);
                return View(new ClienteViewModel { PNatural = natural });
            }
            else
            {
                PersonaJuridicaViewModel juridica = new()
                {
                    IdPersona = cliente.IdCliente,
                    RazonSocial = cliente.RazonSocial ?? "",
                    IdDocumentoIdentidad = cliente.IdDocumentoIdentidad,
                    NumeroDocumento = cliente.NumeroDocumento,
                    CorreoElectronico = cliente.CorreoElectronico,
                    Celular = cliente.Celular
                };
                await GetDataAsync("Edit", id);
                return View(new ClienteViewModel { PJuridica = juridica });
            }
        }

        // POST: Clientes/Edit/1
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ClienteViewModel cliente)
        {
            if (cliente.PNatural == null && cliente.PJuridica == null)
            {
                return BadRequest();
            }
            if ((cliente.PNatural != null && id != cliente.PNatural.IdPersona) | (cliente.PJuridica != null && id != cliente.PJuridica.IdPersona))
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    var strategy = cliente.PJuridica == null ? new PersonaContext(new PersonaNatural()) : new PersonaContext(new PersonaJuridica());
                    await strategy.Edit(cliente, _context, _userInfo);
                    TempData["AlertMsg"] = SweetAlert2.Success("Editado", "Registro editado exitosamente");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClienteExists(cliente))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            await GetDataAsync("Edit", id);
            return View(cliente);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Clientes == null)
            {
                return NotFound();
            }

            var cliente = await _context.Clientes.Include(m => m.IdTipoPersonaNavigation).FirstOrDefaultAsync(m => m.IdCliente == id);
            if (cliente == null)
            {
                return NotFound();
            }

            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Clientes == null)
            {
                return Problem("La entidad 'context' es nula.");
            }
            var cliente = await _context.Clientes.FindAsync(id);
            if (cliente != null)
            {
                cliente.IdEstadoRegistro = HttpContext.User.IsInRole("Cliente") ? 5 : 6;
                cliente.FechaModificacion = DateTime.Now;
                _context.Clientes.Update(cliente);
                //_context.Clientes.Remove(cliente);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        /*
         * Funciones adicionales
         */
        /// <summary>
        /// Funcion para saber si un cliente existe.
        /// </summary>
        /// <param name="cliente">Objeto del Form</param>
        /// <returns>Retorna un valor booleano</returns>
        private bool ClienteExists(ClienteViewModel cliente)
        {
            if (cliente.PJuridica == null && cliente.PNatural != null)
            {
                return (_context.Clientes?.Any(e => e.IdCliente == cliente.PNatural.IdPersona)).GetValueOrDefault();
            }
            if (cliente.PNatural == null && cliente.PJuridica != null)
            {
                return (_context.Clientes?.Any(e => e.IdCliente == cliente.PJuridica.IdPersona)).GetValueOrDefault();
            }
            return false;
        }
        private async Task GetDataAsync(string NameAction, int? idModel = 0)
        {
            ViewBag.DocumentoIdentidadN = new SelectList(await (from d in _context.DocumentoIdentidades where d.Filtro == 1 orderby d.Descripcion select d).AsNoTracking().ToListAsync(), "IdDocumentoIdentidad", "Descripcion");
            ViewBag.DocumentoIdentidadJ = new SelectList(await (from d in _context.DocumentoIdentidades where d.Filtro == 2 orderby d.Descripcion select d).AsNoTracking().ToListAsync(), "IdDocumentoIdentidad", "Descripcion");
            ViewData["DNatural"] = await (from t in _context.TipoPersonas where t.IdTipoPersona == 1 select t.DescripcionCorta).AsNoTracking().FirstAsync();
            ViewData["DJuridica"] = await (from t in _context.TipoPersonas where t.IdTipoPersona == 2 select t.DescripcionCorta).AsNoTracking().FirstAsync();
            // Para evitar referencias NULL
            if (NameAction == "Edit")
            {
                ViewData["Title"] = "Editar registro";
                ViewData["Action"] = "Edit";
                ViewData["Btn"] = "Editar";
                ViewData["Id"] = idModel;
            }
            else
            {
                ViewData["Title"] = "Nuevo registro";
                ViewData["Action"] = "Create";
                ViewData["Btn"] = "Registrar";
                ViewData["Id"] = 0;
            }
        }
        /*
         * Error
         */
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}